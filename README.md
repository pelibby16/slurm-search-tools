# Slurm Search Tools
Created to support running research tasks for SL on the Whedon cluster.

This page contains some general information, resources, as well as terminology definitions and repository setup.

## Contents
- [Project 0 - Getting Started with Experiments](docs/PROJECT0.md)
- [Project 1 - Run a set of Experiments with Slurm](docs/PROJECT1.md)
- [Project 2 - Analyze & Plot Results](docs/PROJECT2.md)

## Code Layout
- `modules/`
  - `search/` - Search Repository [(Link)](https://github.com/snlemons/search).
- `py/`
  - **`generator.py`** - Generates a slurm batch file.
  - **`utils.py`** - Dictionaries and utilities for search code
  - **`runs_slurm.py`** - Takes args from slurm batch and runs actual search code.
- `sbatch/` - batch files are generated to this dir.
  - *`test.sbatch.example`* - sample SBATCH file.
- **`start.py`** - Wrapper for creating and starting a new batch.

## Terminology
1. **Domain** - A domain (or a problem domain) is the specification of what a particular problem can be and how it can be explored. Some examples of domains are grid-based pathfinding and the [sliding tiles puzzle](https://en.wikipedia.org/wiki/Sliding_puzzle). The code for a problem domain includes data for the state of a problem, operators (or actions) that can be taken, costs of those operators, and functions to evaluate a state such as goal tests or heuristic functions. Domains can be found in the [Search Repository](https://github.com/snlemons/search), and as a submodule of this repository in `modules/search`. They must be built with `GCC` or `Clang` before being used.

2. **Cost Model** - Actions within a domain can be specified to have a variety of costs, which we refer to as a cost model. Some examples in the sliding tiles domain are `unit` (where moving any tiles costs 1), or `heavy` (where moving a tile has cost equal to the number of the tile).

3. **Dataset** - A dataset (or problem set) is a grouping of problem instances all belonging to the same domain. Sometimes these are randomized, and sometimes they are selected for some shared feature (ex: grid pathfinding problems might be grouped by percentage of blocked spaces.) Datasets can be found on the Earlham Cluster servers in `/cluster/search-alg-research/instances/` (Read only).

4. **Search Algorithm** - A search algorithm explores the space of possibilities (or state space) of a problem in some methodical way particular to that algorithm. Search algorithm code is written in generalized ways so it should work on any problem domain. This project uses a variety of different algorithms to perform searches, including `A*`, `greedy`, `beam`, and variants of each of these (ex: `weighted A*` or `speedy`.)

## Setup
1. `ssh` to the machine or cluster you want to work on. These steps have been tested on `whedon` so it's recommended you use that for now. Remember that to connect to `whedon` you will need to first connect to an otuward-facing system like `hopper`. ([Instructions](https://wiki.cs.earlham.edu/index.php/Getting_started_on_clusters) for connecting to cluster machines.)
2. Clone this repository using the command `git clone https://code.cs.earlham.edu/pelibby16/slurm-search-tools.git`
3. `cd` into the directory for the cloned repository (by default it will be named `slurm-search-tools` ).
4. From this directory use `git submodule init; git submodule update;` to pull the submodule for search code.
5. `cd` into `modules/search` and use `git checkout main` to switch to the main branch.

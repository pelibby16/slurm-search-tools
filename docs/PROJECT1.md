# Project 1: Run a set of Experiments with Slurm
- [Back to Outline](../README.md)

For this project, you will run a set of experiments using Slurm. If you aren't sure what Slurm is or why we use it, that's ok. Take a look at the `Notes about Slurm` section and ask questions. To put it simply, we use slurm to run lots of things at once, and to automatically run those things on machines with available resources. This takes a lot of the work of planning and queueing experiments off of us, and lets the servers handle it.

We will pick up from the example in [Project 0](PROJECT0.md), where we were using the `tiles` domain, and the `korf100` dataset. Now that we have Slurm at our disposal, we can easily run a similar experiment to project0, but run it for a huge set of different **widths** all at the same time.



**NOTE:** If you have done [Project 0](PROJECT0.md), You will not need to do steps 1-4, since they are already done (unless you need to build a new domain). In this case, start with 5.

1. Follow the [Setup](../README.md#setup) steps before starting this project.

2. `cd modules/search`, and open the `Makefile`. This file has all the details and instructions for how the compiler should build the search code.

3. Check out the `Makefile`, this is what determines how the code is built. Make sure that the `CXX` and `CC` lines correspond to the compiler you want to use. We usually use `gcc`.

4. Use the command `make tiles` to build the program. This will create new executables in the tiles subfolder (`/modules/search/tiles`).

5. (START HERE IF YOU HAVE DONE [PROJECT 0](PROJECT0.md))<br>First, take a look at the examples in the `sbatch/` folder. Notice that each of these scripts includes a copy of the command that was used to generate it. The `start.py` script will generate a similar file, and automatically queue it to Slurm, so make sure you are on Whedon and ready to start experiments when you run it.
Here is one such example:

    ```
    #!/bin/bash
    ## Timestamp: 2022-12-14 13:25:13.445222
    ## Generated by command: python3 start.py -t 5 -s py/runs_slurm.py -v width -d tiles -dt korf100 -cst heavy -c beam

    #SBATCH --job-name="sbatch/run_tiles_korf100_heavy_beam.sbatch"
    #SBATCH --output=sbatch/run_tiles_korf100_heavy_beam.sbatch_%A_%a.out
    #SBATCH --time=5:00:00
    #SBATCH --array=0-7

    module purge
    module load modules
    module load python/3.9

    script=py/runs_slurm.py
    domain=tiles
    dataset=korf100
    cost=heavy
    category=beam
    ind_var=width
    ind_var_list=(100000 30000 10000 3000 1000 300 100 30)
    ind_var_val=${ind_var_list[$((SLURM_ARRAY_TASK_ID))]}

    srun python3 $script $domain $dataset $cost $category $ind_var $ind_var_val &
    wait
    ```
6. Create a `results/` folder with mkdir in the base directory of the repository (`/slurm-search-tools/results`) for the script to store output from each individual experiment.
7. The `start.py` script takes the following arguements:

    - `-e` or `--email` (Not required) - Contact email for slurm. This is the email the slurm job will use to notify you if you job fails, or when it completes.
    - `-t` or `--time` (Not required) - The amount of time that the slurm batch is allowed to run for. Default is 168 hours. 
    - `-s` or `--script` (Required) - The path to the executable you want to run (`tiles/15md_solver` for instance).
    - `-v` or `--var` (Not required) - The independent variable in your experiment.
    - `-cst` or `--cost` (Required) - Which cost model to use in the experiment.
    - `-c` or `--category` (Required) - Which category to use for the experiment (`beam`, for example).
    - `-d` or `--domain` (Required) - Which domain to use for the experiment.
    - `-dt` or `--dataset` (Required) - Which dataset to use for the experiment.
8. Try the following commands, and remember that you can check the progress of these as they run by using the `squeue` command (they make take several minutes):
    - `python3 start.py -t 5 -s py/runs_slurm.py -d tiles -dt korf100 -cst unit -c gbfs`
    - `python3 start.py -t 5 -s py/runs_slurm.py -v width -d tiles -dt korf100 -cst unit -c beam`
    - `python3 start.py -t 5 -s py/runs_slurm.py -d tiles -dt korf100 -cst heavy -c gbfs`
    - `python3 start.py -t 5 -s py/runs_slurm.py -v width -d tiles -dt korf100 -cst heavy -c beam`
9. Take a look at your output in `results/tiles/`. There should be a lot of individual result files. Their names are descriptive, so the `grep` command is a useful tool here. For example:
    - `ls | grep unit` will show all the result files produced using the unit cost model. 
    - `ls -la | grep heavy | wc -l` will show you how many results you have that were run with the heavy cost model.
# Notes about Slurm
## What is it?
Slurm is an open source, scalable cluster management and job scheduling system for large and small Linux clusters. It allow us, as users, to queue up jobs (such as experiments like this). Since the software is in charge of what is being run and where, we can queue up more jobs than the computer has resources. 

For example, say you wanted to run a set of 20 scripts that needs 4GB of RAM each to run, but your computer only has 16GB of RAM. Slurm will hold these jobs in a queue, and run them as the resources are available.

At Earlham, we use Slurm to manage jobs on multiple machines at once, with lots of users queuing different jobs. Since Slurm is managing everything, there is no need to start experiments one at a time. You can queue them all once, and then check back later to see the results.

If you want to learn more, Slurm has a great [documentation page](https://slurm.schedmd.com/overview.html).

## Useful Commands
- `sinfo` - This command will show you basic information about all the nodes (machines) that slurm is in charge of, and what their status is (busy, idle, offline, etc).
- `squeue` - This command will show you the jobs that are currently waiting to be run, or are in the process of being run.
- `srun [script] [args...]` - This command tells slurm to queue a single job.
- `sbatch [sbatch file]` - This command tells slurm to queue a set of jobs, based on the parameters set in an `sbatch` file.

# Project 0: Getting Started with Experiments
- [Back to Outline](../README.md)

**Reminder:** You will need to build the code on a machine with the appropriate libraries installed. The steps have been tested on `whedon`, so that is the recommended system to use.

For this first project, you will run a single experiment in the `tiles` domain, using the `korf100` dataset.
1. Follow the [Setup](../README.md#setup) steps before starting this project.
2. `cd modules/search`, and open the `Makefile`. This file has all the details and instructions for how the compiler should build the search code.
3. Check out the `Makefile`, this is what determines how the code is built. Make sure that the `CXX` and `CC` lines correspond to the compiler you want to use. We usually use `gcc`.
   ```
   CXX:=g++    <- These lines tell the Makefile to use GCC
   CC:=gcc     <- (uncommented)

   #CXX:=clang++     <- These lines tell the Makefile to use Clang
   #CC:=clang        <- (commented out)
   ```

4. Use the command `make tiles` to build the program. This will create new executables in the tiles subfolder (`/modules/search/tiles`). It will take a minute to run.

5. Now you can use the executable you just compiled to run experiments in that domain. Before the complete command, lets break down what information we need to test.

   1. Domain - We've just built the `tiles` domain, so we can use one of those. For this example, we will use the `15md_solver`, which solves 4 by 4 sliding tile puzzles. We know from earlier where this executable is located, so we will want to use that path to call it. From the `/modules/search` folder we have been working in, the path is `tiles/15md_solver`.

   2. Cost - Many domains have more than one cost model (the costs of actions you can take when solving the problems.) We specify these by using `-cost` and thename of the cost model (ex: `-cost unit`). In tiles, we have the following cost models:
   * unit - Moving any tile costs 1.
   * heavy - Moving a tile has cost equal to the number on the tile (ex: moving tile 4 costs 4).
   * reverse - Moving a tile has cost equal to the size of the puzzle minus the number of the tile (ex: moving tile 5 in a 4 by 4 puzzle has cost `16 - 4 = 12`).
   * sqrt - Moving a tile has cost equal to the square root of the number on the tile (ex: moving tile 4 has cost 2).
   * inverse - Moving a tile has cost equal to 1 divided by the number on the tile (ex: moving tile 4 costs `1/4`).
   * revinv - Reverse inverse. Moving a tile has cost equal to 1 divided by the size of the puzzle minus the number on the tile (ex: moving tile 4 in a 4 by 4 puzzle costs `1/(16 - 4) = 1/12`).

   3. Search Algorithm - Now we need to choose an algorithm to search for a solution. For this example, we will use `beam`, but there are many other options.

   4. Arguments - We can specify a few additional arguments for how we are running the experiment. These can very greatly with different domains and algorithms. Since we are using `beam`, we will need to specify a `-width` argument. This is an easy parameter to experiment with, but for our first test, lets use `100`. We can also specify a `-mem` argument, which can be used to keep the experiment from getting out of control in the case that something goes wrong.

   4. Finally, we need to specify an instance of the domain that we want to use for our test. You can look at the full list of instances on cluster.earlham.edu in `/cluster/search-alg-research/instances`. For this example, we can use one of the `korf100` instances. 

6. Our final command looks like this: `tiles/15md_solver beam -width 100 -cost heavy -mem 7.5G < /cluster/search-alg-research/instances/korf100/1`
   
7. Note the output of the program, and the various pieces of information you have collected with your experiment. Later on, these pieces of information can be used for analysis and visualization. Notice that the output is all pairs; a label and a value. It should look something like this: 
   ```
   #start data file format 4
   #pair  "wall start date"	"Fri Dec  9 14:49:18 2022"
   #pair  "wall start time"	"1.67062e+09"
   #pair  "machine id"	"w0.whedon.loc-Linux-3.10.0-1160.76.1.el7.x86_64-x86_64"
   #pair  "cost"	"heavy"
   #pair  "initial heuristic"	"353.000000"
   #pair  "initial distance"	"41.000000"
   #pair  "algorithm"	"beam"
   #pair  "final sol cost"	"213541.000000"
   #pair  "memory limit"	"7500000000"
   #pair  "state size"	"76"
   #pair  "packed state size"	"8"
   #pair  "final sol length"	"63232"
   #pair  "total raw cpu time"	"3.357767"
   #pair  "total wall time"	"3.357768"
   #pair  "total nodes expanded"	"6322653"
   #pair  "total nodes generated"	"13127420"
   #pair  "total nodes duplicated"	"1327030"
   #pair  "total nodes reopened"	"30916"
   #pair  "closed list type"	"hash table"
   #pair  "closed fill"	"6291737"
   #pair  "closed collisions"	"614697"
   #pair  "closed resizes"	"0"
   #pair  "closed buckets"	"30000001"
   #pair  "closed max bucket fill"	"6"
   #pair  "open list type"	"binary heap"
   #pair  "node size"	"48"
   #pair  "wall finish time"	"1.67062e+09"
   #pair  "wall finish date"	"Fri Dec  9 14:49:22 2022"
   #pair  "max virtual kilobytes"	"721084"
   #end data file format 4
   ```
8. Examine some specific lines from the output. Each line tells you different information about the solution found or the algorithm's performance. For example:

* `#pair  "final sol cost"	"213541.000000"` - the solution found had cost 213541. **If the cost shown is `-1`, then the algorithm did not solve the problem.**
* `#pair  "final sol length"	"63232"` - the solution found had 63232 states in it (which means it took 63231 actions after the start state).
* `#pair  "total raw cpu time"	"3.357767"` - the algorithm's execution took 3.357767 seconds.
* `#pair  "total nodes expanded"	"6322653"` - the algorithm explored 6322653 possibilities.

9. Try a substituting a few different parameters. What happens with a smaller `-width` parameter? What if you use a different `-cost`, such as `unit`? Is the output the same if you run the same exact command multiple times? See if you can learn anything from the differences in the output.

# Project 2: Analyze & Plot Results
- [Back to Outline](../README.md)
- [Matplotlib Examples](https://matplotlib.org/stable/gallery/index.html)

For this project, you will create a graph of data collected in your experiments. The file `py/plot_example.py` can create some simple plots, but it is more of a template than a tool. Most experiments will require a more custom approach to graphing in order to visualize useful information. 

Note: If you are doing this task on Earlham servers, **you will want to be connected to hopper**, not one of the whedon nodes. If you would like to do your visualizations in Jupyter notebooks, you can use the [Cluster Jupyterlab server](https://jupyter.cluster.earlham.edu) on Layout. Layout uses the same filesystem as Whedon, so all your code and results will be available just as they are there.

1. Take a look at `py/plot_example.py`, read through the code and comments to get an understanding of what is happening. If you are unfamiliar with _matplotlib_ syntax, now would be a good time to read the docs, or practice a bit so that you have an understanding of what goes where.

2. Make sure that you have a python version with matplotlib available. The necessary libraries on whedon are already installed for version 3.9 . To ensure you're running this version, use the command `module load python/3.9` . You might consider adding this at the bottom of your `~/.bashrc` file to make sure you are always using this same version of python.

3. Try using the script to generate some plots. These require you to have specific data in your `results/` folder, so you may have to go back and run some more tests to gather all the data you need. If you don't have that data, the script will inform you of what you are missing. Here are some examples of how to run the script, which assume you are running from the main repository directory (by default `slurm-search-tools`).  
  Example 1: `python py/plot_example.py tiles unit`   
  Example 2: `python py/plot_example.py tiles heavy`

4. See if you can find a reasonable explanation for why the graph looks like this. Is there a particular experiment or `width` that performed the best? 

5. Edit `plot_example.py` and change what data is being graphed. The table below outlines some of the information that you can use. There are others, but these are the ones that are useful for this kind of plot. Try to use these to prove or disprove your hypothesis from earlier, and remember to update labels when you change axis data. The `x` and `y` axis data can be modified by changing the variables on lines 14-17:
```python
xdata = data.cpu_times
ydata = data.costs
xlabel = "CPU time"
ylabel = "solution cost"

```

| Data variable | Description | Type |
| --- | --- | --- |
| costs | The total cost of the solution | (list) int/float |
| sol_lengths | The total length of the solution | (list) int |
| cpu_times | Total CPU time execution | (list) float |
| wall_times | Total real-time execution | (list) float |
| expanded | Number of nodes that were explored. | (list) int |
| generated | Number of nodes that were created. | (list) int |
| num_sols | Number of solutions returned | (list) int |
| mem_fails | Number of instances that ran out of memory | int |
| deadend_fails | Number of instances that ran out of unexplored states | int |

6. Consider other algorithms, domains, and cost models. Now you have all the tools you need to try new experiments and compare the behaviors and results.


## Notes and Tips

- Some algorithm configurations may not solve all problems given to them. You can use the data given by `read_data` to detect this, but you should consider what to do when presenting data with failed runs. For example, what should you report as the cost or length average when an algorithm only solves some of the problems?

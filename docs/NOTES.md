# Notes
[-> Google Doc](https://docs.google.com/document/d/1_uGcTkxQNSv4SaFGXhvDpHhnoBptAvIagal2Jo_FJCM/edit)

[-> Search Repo](https://github.com/snlemons/search)
- data -> domain
- results file, highlight some things that could be interesting to plot, nodes expanded, walltime, etc.
- consistent path for instances 

## Example
- domain: tiles, dataset: korf100

## To Do
- slurm script: need to update generator to use util.py data structure for ind variable.
- Create a Tutorial/introduction with steps
  - simple tutorial, pull and build
  - create a diagram notes/tutorial
  - using with slurm / running multiple tasks
- layout diagram of how these things interact
  - diagram of file structure
  - diagram of domains / datasets?
- Add sofia's code as a submodule, so that it can be updated.
  - ~~search code repository~~
  - vis tools?

## Slurm Script Samples
`python start.py -t 5 -s test.py -v width -d domain1 -dt dataset1 -c cost1`

## Template SBATCH
```
#!/bin/bash
#SBATCH --job-name="unit"
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=pelibby16@earlham.edu
#SBATCH --output=unit_%A_%a.out
#SBATCH --time=05:00:00
#SBATCH --array=0-6

module purge
module load modules
module load python/3.9

script=runs-slurm.py
domain=11tiles
dataset=11tiles
cost=unit
ind_var=width
ind_var_list=(3 10 30 100 300 1000 3000)
ind_var_val=${ind_var_list[$((SLURM_ARRAY_TASK_ID))]}

srun python3 $script $domain $dataset $cost $ind_var $ind_var_val &

# python3 runs-slurm.py 11tiles 11tiles unit width 3 &
wait
```
#!/usr/bin/python3

import subprocess
import sys
from os import listdir, mkdir
import utils


n_val = 30

# Use these lines if you want to test every width value.
#mx = 1000
#widths = range(1,mx+1)

alg_categories = {"beam": ["beam", "bead"],
                  "monobeam": ["monobeam", "monobead"],
                  "gbfs": ["greedy", "speedy"],
                  "arastar":["arastar"],
                  "triangle":["triangle"],
                  "rectangle":["rectangle"]}


def runs(domain, dataSet, cst, arg, argval, algs, nInst=100, startInst=1, ea=None, ev=None):
    #print("Starting")

    exec_dir = "modules/search/"+utils.domain_dir[domain]+"/"    # small edit for the local repo copies
    exec_file = utils.exec_files[domain]
    data_dir = "/cluster/search-alg-research/instances/"+dataSet+"/"
    timelimit = 5*60
    memlimit = "7.5G"
    dups = True

    if domain == "synth_tree":
        extra_opt = "-err"
        extra_val = "0.2"
    elif domain == "12pancake":
        extra_opt = "-gap"
        extra_val = str(ev)
    elif ev == "dump":
        extra_opt = "-dump"
        extra_val = ""
    elif ea != None:
        extra_opt = ea
        extra_val = ev
    else:
        extra_opt = ""
        extra_val = ""

    try:
        mkdir("results/")
    except:
        pass

    try:
        mkdir("results/"+domain+"/")
    except:
        pass
        
    for i in range(startInst, startInst + nInst):
        file = str(i)
        sys.stderr.write("File: " + file + "\n")
        #print(file)
        file_results = {}
        best_cost = -1
        for alg in algs:
            
            sys.stderr.write("Algorithm: " + alg + "\n")
            if not dups: print("Duplicate dropping")
            dupString = ["-dropdups", ""][dups]
            
            sys.stderr.write("Arg: " + str(argval) + "\n")
            cmd = [exec_dir+exec_file, alg,
                   arg, str(argval),
                   "-walltime", str(timelimit),
                   "-mem", memlimit,
                   "-cost", cst,
                   extra_opt, extra_val,
                   dupString] # some of these may be empty strings
            sys.stderr.write(" ".join(cmd)+"\n")

            inPath = data_dir + file + ["", "-"+cst][domain=="gridnav"]
            outfile = open("results/"+domain+"/"+str(file)+alg
                           +dupString+arg+str(argval)
                           +"-"+cst+extra_opt+extra_val,
                           "w")

            process = subprocess.Popen(cmd,
                                       stdin=open(inPath, "r"),
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.STDOUT)
            stdout, stderr = process.stdout, process.stderr
            lines = [line.decode("utf-8") for line in stdout.readlines()]
            outfile.writelines(lines)
            try:
                cost_line = [x for x in lines if "final sol cost" in x][0]
                cost = float(cost_line.split("\"")[3])
            except:
                sys.stderr.write("Failed file: " + file + "\n")
            

if __name__ == "__main__":
    domain = sys.argv[1]
    dataSet = sys.argv[2]
    cst = sys.argv[3]
    category = sys.argv[4]
    if len(sys.argv) > 5:
        arg = "-"+sys.argv[5]
        if len(sys.argv) > 6:
            argval = sys.argv[6]
        else:
            argval = ""
    else:
        arg = ""
        argval = ""
    if len(sys.argv) > 7:
        ea = "-"+sys.argv[7]
        ev = sys.argv[8]
    else:
        ea = ""
        ev = ""
    startInst = 1

    # USE THIS ONLY FOR DUMPING ALL STATES. COMMENT OUT OTHERWISE
    # ev = "dump"

    nInst=utils.domain_instances[domain]
    algs = alg_categories[category]
    if cst == "unit" and category in ["beam", "gbfs", "monobeam"]:
        algs = algs[:1]
    
    runs(domain, dataSet, cst, arg, argval, algs, nInst, startInst=startInst, ea=ea, ev=ev)

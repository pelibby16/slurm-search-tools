import argparse
import py.generator as generator 
import sys
from os import system

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # save full command
    full_command_ls = sys.argv
    full_command = "python3 " + " ".join(full_command_ls)

    # SBATCH ARGS
    parser.add_argument("-e", "--email", help="Enter an email. If left blank, no email output will be used.")
    parser.add_argument("-t", "--time", help="Enter the max time for the job in hours (int)", default="168")
    
    # SCRIPT ARGS
    parser.add_argument("-s", "--script", help="Enter the relative path to the script you would like to run.", required=True)
    parser.add_argument("-v", "--var", help="Enter the independent variable for the test.", required=False)
    parser.add_argument("-cst", "--cost", help="Enter the cost model you would like to use.", required=True)
    parser.add_argument("-c", "--category", help="cost model category.", required=True)
    parser.add_argument("-d", "--domain", help="Enter the domain to be used.", required=True)
    parser.add_argument("-dt", "--dataset", help="Enter the dataset to be used.",required=True)
    
    # CREATE SBATCH
    args = parser.parse_args()    
    batch_name = generator.create_sbatch(args, full_command)
    system("sbatch " + batch_name)

    # RUN SBATCH
    # tbd